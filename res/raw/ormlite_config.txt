#
# generated on 2012/06/29 07:50:40
#
# --table-start--
dataClass=core.History
tableName=history
# --table-fields-start--
# --field-start--
fieldName=problemInternalId
id=true
indexName=history_problemInternalId_idx
# --field-end--
# --field-start--
fieldName=date
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=core.Source
tableName=source
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
indexName=source_id_idx
allowGeneratedIdInsert=true
# --field-end--
# --field-start--
fieldName=name
# --field-end--
# --field-start--
fieldName=url
uniqueIndexName=source_url_idx
# --field-end--
# --field-start--
fieldName=uploadSupported
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=core.Problem
tableName=problem
# --table-fields-start--
# --field-start--
fieldName=position
# --field-end--
# --field-start--
fieldName=nom
# --field-end--
# --field-start--
fieldName=description
# --field-end--
# --field-start--
fieldName=id
# --field-end--
# --field-start--
fieldName=source
# --field-end--
# --field-start--
fieldName=nbMoves
# --field-end--
# --field-start--
fieldName=resolu
# --field-end--
# --field-start--
fieldName=internalId
generatedId=true
indexName=problem_internalId_idx
allowGeneratedIdInsert=true
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.estragon.sql.SimpleData
tableName=simpledata
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=string
indexName=simpledata_string_idx
# --field-end--
# --field-start--
fieldName=millis
# --field-end--
# --field-start--
fieldName=date
# --field-end--
# --field-start--
fieldName=even
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.estragon.sql.Account
tableName=accounts
# --table-fields-start--
# --field-start--
fieldName=name
id=true
# --field-end--
# --field-start--
fieldName=password
# --field-end--
# --table-fields-end--
# --table-end--
#################################
